package hu.aquajava.expensetracker.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity
data class Expense(
    @PrimaryKey(autoGenerate = true) val id: Long,
    @ColumnInfo(name = "title") val title: String?,
    @ColumnInfo(name = "date") val date: Date,
    @ColumnInfo(name = "amount") val amount: Double,
){
    constructor(title: String, date: Date, amount: Double) : this(0, title, date, amount)
}