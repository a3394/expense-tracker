package hu.aquajava.expensetracker.util

import android.icu.text.NumberFormat
import java.util.*

class CurrencyFormatter {
    companion object {
        private val currencyFormat = NumberFormat.getCurrencyInstance(Locale("hu", "HU"))

        fun format(number: Double): String = currencyFormat.format(number)
    }

}