package hu.aquajava.expensetracker.util

import android.app.Dialog
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment

class ConfirmationAlertDialogFragment: DialogFragment() {

    companion object {
        const val TAG = "ConfirmationAlertDialog"
        const val KEY_MESSAGE = "message"
        const val KEY_POSITIVE_BUTTONTITLE = "positiveButtonTitle"
        const val KEY_NEGATIVE_BUTTONTITLE = "negativeButtonTitle"

        fun newInstance(message: String, positiveButton: String, negativeButton: String): ConfirmationAlertDialogFragment {
            val args = Bundle()
            args.putString(KEY_MESSAGE, message)
            args.putString(KEY_POSITIVE_BUTTONTITLE, positiveButton)
            args.putString(KEY_NEGATIVE_BUTTONTITLE, negativeButton)

            val fragment = ConfirmationAlertDialogFragment()
            fragment.arguments = args
            return fragment
        }
    }

    var dialogResult: ((Boolean) -> Unit)? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog =
        AlertDialog.Builder(requireContext())
            .setMessage(arguments?.getString(KEY_MESSAGE))
            .setPositiveButton(arguments?.getString(KEY_POSITIVE_BUTTONTITLE)) { _, _ ->
                dialogResult?.let { it(true) }
            }
            .setNegativeButton(arguments?.getString(KEY_NEGATIVE_BUTTONTITLE)) { _, _ ->
                dialogResult?.let { it(false) }
            }
            .create()

}