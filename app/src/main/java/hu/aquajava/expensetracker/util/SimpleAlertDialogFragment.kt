package hu.aquajava.expensetracker.util

import android.app.Dialog
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment

class SimpleAlertDialogFragment : DialogFragment() {

    companion object {
        const val TAG = "SimpleAlertDialog"
        const val KEY_MESSAGE = "message"
        const val KEY_BUTTONTITLE = "buttonTitle"

        fun newInstance(message: String, buttonTitle: String): SimpleAlertDialogFragment {
            val args = Bundle()
            args.putString(KEY_MESSAGE, message)
            args.putString(KEY_BUTTONTITLE, buttonTitle)

            val fragment = SimpleAlertDialogFragment()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog =
        AlertDialog.Builder(requireContext())
            .setMessage(arguments?.getString(KEY_MESSAGE))
            .setPositiveButton(arguments?.getString(KEY_BUTTONTITLE)) { _, _ -> }
            .create()
}