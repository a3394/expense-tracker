package hu.aquajava.expensetracker.util

import java.text.DateFormat
import java.util.*

class DateFormatter {
    companion object {
        fun format(date: Date): String {
            val df = DateFormat.getDateInstance(DateFormat.MEDIUM)
            return df.format(date)
        }
    }

}