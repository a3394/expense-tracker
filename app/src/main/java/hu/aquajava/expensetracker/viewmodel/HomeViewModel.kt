package hu.aquajava.expensetracker.viewmodel

import android.database.sqlite.SQLiteException
import android.icu.text.NumberFormat
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import hu.aquajava.expensetracker.db.ExpensesRepo
import hu.aquajava.expensetracker.model.Expense
import hu.aquajava.expensetracker.util.CurrencyFormatter
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(private val expensesRepo: ExpensesRepo): ViewModel() {
    private val _allExpenses: MutableLiveData<List<Expense>> = MutableLiveData()
    private val _calculatedSum: MutableLiveData<Double> = MutableLiveData()
    private val _itemDelete: MutableLiveData<Boolean> = MutableLiveData()

    val allExpenses: LiveData<List<Expense>> = _allExpenses
    val calculatedSum: LiveData<Double> = _calculatedSum
    val itemDelete: LiveData<Boolean> = _itemDelete

    fun getExpenses() {
        viewModelScope.launch {
            val expenses = expensesRepo.getExpenses()
            _allExpenses.value = expenses
        }
    }

    fun deleteExpense(expense: Expense) {
        viewModelScope.launch {
            try {
                expensesRepo.deleteExpense(expense)
                _itemDelete.value = true
            } catch (e: SQLiteException) {
                Log.w(HomeViewModel::class.java.simpleName, "Delete failed", e)
                _itemDelete.value = false
            }

        }
    }

    fun calcTotalExpenses() {
        viewModelScope.launch {
            var total = 0.0
            val expenses = expensesRepo.getExpenses()

            expenses.forEach {
                total += it.amount
            }

            _calculatedSum.value = total
        }

    }

}