package hu.aquajava.expensetracker.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import hu.aquajava.expensetracker.db.ExpensesRepo
import hu.aquajava.expensetracker.model.Expense
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject

@HiltViewModel
class CreateExpenseViewModel @Inject constructor(val expensesRepo: ExpensesRepo): ViewModel() {

    private val _addState = MutableLiveData<Boolean>()
    val addState: LiveData<Boolean> = _addState

    fun addExpense(title: String, amount: Double) {
        val expense = Expense(title = title, amount = amount, date = Date())

        viewModelScope.launch {
            expensesRepo.addExpense(expense)

            _addState.value = true
        }

    }

}