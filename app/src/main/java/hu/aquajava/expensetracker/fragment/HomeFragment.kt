package hu.aquajava.expensetracker.fragment

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import dagger.hilt.android.AndroidEntryPoint
import hu.aquajava.expensetracker.Navigation
import hu.aquajava.expensetracker.R
import hu.aquajava.expensetracker.adapter.ExpenseListAdapter
import hu.aquajava.expensetracker.databinding.FragmentHomeBinding
import hu.aquajava.expensetracker.util.CurrencyFormatter
import hu.aquajava.expensetracker.viewmodel.HomeViewModel

@AndroidEntryPoint
class HomeFragment: BaseFragment() {

    companion object {
        fun newInstance(): HomeFragment {
            val args = Bundle()

            val fragment = HomeFragment()
            fragment.arguments = args
            return fragment
        }
    }

    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!

    private val model: HomeViewModel by viewModels()

    private val listAdapter = ExpenseListAdapter()

    private val itemTouchHelperCallback = object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {

        val background = ColorDrawable(Color.RED)

        override fun onMove(
            recyclerView: RecyclerView,
            viewHolder: RecyclerView.ViewHolder,
            target: RecyclerView.ViewHolder
        ) = false

        override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
            val pos = viewHolder.absoluteAdapterPosition
            val item = listAdapter.currentList[pos]

            Navigation.showConfirmationAlert(childFragmentManager, getString(R.string.dialog_delete_confirmation), getString(R.string.dialog_button_yes), getString(R.string.dialog_button_no)) {
                if (it) {
                    model.deleteExpense(item)
                } else {
                    listAdapter.notifyDataSetChanged()
                }
            }
        }

        override fun onChildDraw(
            c: Canvas,
            recyclerView: RecyclerView,
            viewHolder: RecyclerView.ViewHolder,
            dX: Float,
            dY: Float,
            actionState: Int,
            isCurrentlyActive: Boolean
        ) {
            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)

            val itemView = viewHolder.itemView
            background.setBounds(itemView.right + dX.toInt(), itemView.top, itemView.right, itemView.bottom)
            background.draw(c)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        model.allExpenses.observe(this, { expenses ->
            listAdapter.submitList(expenses)
        })

        model.calculatedSum.observe(this, {
            _binding?.totalAmount?.text = CurrencyFormatter.format(it)
        })

        model.itemDelete.observe(this, { result ->
            if (result) {
                model.getExpenses()
                model.calcTotalExpenses()
            } else {
                Navigation.showAlert(childFragmentManager, getString(R.string.dialog_delete_failed_message), getString(R.string.dialog_button_ok))
            }
        })
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val layoutManager = LinearLayoutManager(context)
        layoutManager.orientation = LinearLayoutManager.VERTICAL
        binding.recycleView.layoutManager = layoutManager
        binding.recycleView.setHasFixedSize(true)
        binding.recycleView.addItemDecoration(DividerItemDecoration(context, layoutManager.orientation))
        binding.recycleView.adapter = listAdapter

        val itemTouchHelper = ItemTouchHelper(itemTouchHelperCallback)
        itemTouchHelper.attachToRecyclerView(binding.recycleView)

        binding.fab.setOnClickListener {
            Navigation.showCreateExpensePage(parentFragmentManager)
        }
    }

    override fun onStart() {
        super.onStart()

        model.getExpenses()
        model.calcTotalExpenses()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        activity?.title = getString(R.string.app_name)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}