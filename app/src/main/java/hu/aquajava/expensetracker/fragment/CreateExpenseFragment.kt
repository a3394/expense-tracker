package hu.aquajava.expensetracker.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import dagger.hilt.android.AndroidEntryPoint
import hu.aquajava.expensetracker.Navigation
import hu.aquajava.expensetracker.R
import hu.aquajava.expensetracker.databinding.FragmentCreateExpenseBinding
import hu.aquajava.expensetracker.viewmodel.CreateExpenseViewModel

@AndroidEntryPoint
class CreateExpenseFragment: BaseFragment() {
    companion object {
        fun newInstance(): CreateExpenseFragment {
            val args = Bundle()

            val fragment = CreateExpenseFragment()
            fragment.arguments = args
            return fragment
        }
    }

    private var _binding: FragmentCreateExpenseBinding? = null
    private val binding get() = _binding!!

    val model: CreateExpenseViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        model.addState.observe(this, {
            Navigation.goBack(parentFragmentManager)
        })
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentCreateExpenseBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.saveButton.setOnClickListener {
            val title = binding.expenseTitleEdit.text.toString()
            val amount = binding.expenseAmountEdit.text.toString().toDouble()
            model.addExpense(title, amount)
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        activity?.title = getString(R.string.create_expense_title)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}