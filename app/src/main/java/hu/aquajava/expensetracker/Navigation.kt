package hu.aquajava.expensetracker

import androidx.fragment.app.FragmentManager
import hu.aquajava.expensetracker.fragment.CreateExpenseFragment
import hu.aquajava.expensetracker.fragment.HomeFragment
import hu.aquajava.expensetracker.util.ConfirmationAlertDialogFragment
import hu.aquajava.expensetracker.util.SimpleAlertDialogFragment

class Navigation {

    companion object {
        fun start(manager: FragmentManager) {
            if (manager.fragments.size == 0) {
                manager.beginTransaction()
                    .replace(R.id.fragment_content, HomeFragment.newInstance()).commit()
            }
        }

        fun showHomePage(manager: FragmentManager) {
            clearBackstack(manager)
            manager.beginTransaction().replace(R.id.fragment_content, HomeFragment.newInstance()).commit()
        }

        fun showAlert(manager: FragmentManager, message: String, buttonTitle: String) {
            SimpleAlertDialogFragment.newInstance(message, buttonTitle).show(manager, SimpleAlertDialogFragment.TAG)
        }

        fun showConfirmationAlert(manager: FragmentManager, message: String, positiveButton: String, negativeButton: String, result: (Boolean) -> Unit) {
            val dialog = ConfirmationAlertDialogFragment.newInstance(message, positiveButton, negativeButton)
            dialog.dialogResult = result
            dialog.show(manager, SimpleAlertDialogFragment.TAG)
        }

        fun showCreateExpensePage(manager: FragmentManager) {
            manager.beginTransaction().replace(R.id.fragment_content, CreateExpenseFragment.newInstance()).addToBackStack("createExpense").commit()
        }

        fun goBack(manager: FragmentManager) {
            manager.popBackStack()
        }

        private fun clearBackstack(manager: FragmentManager) {
            for (i in 0 until manager.backStackEntryCount) {
                manager.popBackStack()
            }
        }
    }

}