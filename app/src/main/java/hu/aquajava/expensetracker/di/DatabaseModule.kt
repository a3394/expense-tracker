package hu.aquajava.expensetracker.di

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import hu.aquajava.expensetracker.DATABASE_NAME
import hu.aquajava.expensetracker.db.AppDatabase
import hu.aquajava.expensetracker.db.ExpenseDao
import hu.aquajava.expensetracker.db.ExpensesRepo
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class DatabaseModule {
    @Provides
    fun provideExpenseDao(appDatabase: AppDatabase) = appDatabase.expenseDao()

    @Provides
    fun provideExpensesRepo(expenseDao: ExpenseDao): ExpensesRepo = ExpensesRepo(expenseDao)

    @Provides
    @Singleton
    fun provideAppDatabase(@ApplicationContext appContext: Context): AppDatabase {
        return Room.databaseBuilder(
            appContext,
            AppDatabase::class.java,
            DATABASE_NAME
        ).build()
    }
}