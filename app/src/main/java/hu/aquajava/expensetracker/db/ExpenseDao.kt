package hu.aquajava.expensetracker.db

import androidx.room.*
import hu.aquajava.expensetracker.model.Expense

@Dao
interface ExpenseDao {
    @Query("SELECT * FROM expense ORDER BY date DESC")
    suspend fun getAll(): List<Expense>

    @Query("SELECT * FROM expense WHERE id = :id")
    suspend fun getExpense(id: Int): Expense?

    @Insert
    suspend fun insertAll(vararg expenses: Expense)

    @Delete
    suspend fun delete(expense: Expense)

    @Update
    suspend fun update(vararg expenses: Expense)

}