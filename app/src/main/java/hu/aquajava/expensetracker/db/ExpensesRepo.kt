package hu.aquajava.expensetracker.db

import hu.aquajava.expensetracker.model.Expense
import javax.inject.Inject

class ExpensesRepo @Inject constructor(val expenseDao: ExpenseDao) {

    suspend fun getExpenses() = expenseDao.getAll()

    suspend fun getExpense(id: Int) = expenseDao.getExpense(id)

    suspend fun addExpense(expense: Expense) {
        expenseDao.insertAll(expense)
    }

    suspend fun updateExpense(expense: Expense) {
        expenseDao.update(expense)
    }

    suspend fun deleteExpense(expense: Expense) {
        expenseDao.delete(expense)
    }
}