package hu.aquajava.expensetracker.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import hu.aquajava.expensetracker.R
import hu.aquajava.expensetracker.model.Expense
import hu.aquajava.expensetracker.util.CurrencyFormatter
import hu.aquajava.expensetracker.util.DateFormatter

class ExpenseListAdapter: ListAdapter<Expense, ExpenseListAdapter.ExpenseViewHolder>(ExpenseItemCallback) {
    var itemSelected: ((Expense) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExpenseViewHolder {
        return ExpenseViewHolder.create(parent) { index ->
            itemSelected?.let { it(currentList[index]) }
        }
    }

    override fun onBindViewHolder(holder: ExpenseViewHolder, position: Int) {
        val current = getItem(position)
        holder.bind(current)
    }

    companion object {
        private val ExpenseItemCallback = object : DiffUtil.ItemCallback<Expense>() {

            override fun areItemsTheSame(oldItem: Expense, newItem: Expense): Boolean {
                return oldItem === newItem
            }

            override fun areContentsTheSame(oldItem: Expense, newItem: Expense): Boolean {
                return oldItem.id == newItem.id
            }

        }
    }

    class ExpenseViewHolder(itemView: View, val itemSelected: (Int) -> Unit) : RecyclerView.ViewHolder(itemView) {
        private val expenseDate: TextView = itemView.findViewById(R.id.expense_date)
        private val expenseTitle: TextView = itemView.findViewById(R.id.expense_title)
        private val expenseAmount: TextView = itemView.findViewById(R.id.expense_amount)

        init {
            itemView.setOnClickListener {
                itemSelected(absoluteAdapterPosition)
            }
        }

        fun bind(expense: Expense) {
            expenseDate.text = DateFormatter.format(expense.date)
            expenseTitle.text = expense.title
            expenseAmount.text = CurrencyFormatter.format(expense.amount)
        }

        companion object {
            fun create(parent: ViewGroup, itemSelected: (Int) -> Unit): ExpenseViewHolder {
                val view: View = LayoutInflater.from(parent.context)
                    .inflate(R.layout.expense_item, parent, false)
                return ExpenseViewHolder(view, itemSelected)
            }
        }
    }
}