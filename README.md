# Expense Tracker

Your app will track personal expenses. It should be able to create and save expenses, and display a grand total of money spent.

## Installation

Clone the project, open in Android Studio and build/run it.